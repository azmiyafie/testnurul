package com.example.myapplication.service
import com.example.myapplication.model.ResponeDetailTopStory
import com.example.myapplication.model.ResponeTopStory
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.Url
import retrofit2.http.GET
import retrofit2.http.Streaming

interface API {
    @GET("askstories.json?print=pretty")
    fun topStoryRequest(
        /*@Body body: SignInRequest?,
        @Header("lat") lat: String?,
        @Header("long") long: String?,
        @Header("auditaction") auditaction: String?*/): Call<ArrayList<String>>

    @GET("item/{id}.json?print=pretty")
    fun getDetailStory(@Path("id") id:Int):Call<ResponeDetailTopStory>
}

