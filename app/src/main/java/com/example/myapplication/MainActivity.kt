package com.example.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.model.ResponeDetailTopStory
import com.example.myapplication.model.ResponeTopStory
import com.example.myapplication.service.API
import com.example.myapplication.service.Retrofit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        GetData()


    }

    private fun GetData() {

        val api = Retrofit.create(this)
        api.topStoryRequest().enqueue(object : Callback<ArrayList<String>> {
            override fun onFailure(call: Call<ArrayList<String>>, t: Throwable) {
                println("Gagal")
            }

            override fun onResponse(call: Call<ArrayList<String>>, response: Response<ArrayList<String>>
            ) {
                println("Result:${response.body()}")
                val resultListId = response.body()?.get(0)
                getIdStory(resultListId!!.toInt())

            }

        })
    }
    private fun getIdStory(id :Int) {
        val api = Retrofit.create(this)
        api.getDetailStory(id).enqueue(object : Callback<ResponeDetailTopStory> {
            override fun onFailure(call: Call<ResponeDetailTopStory>, t: Throwable) {
                println("Gagal")
            }
            override fun onResponse(
                call: Call<ResponeDetailTopStory>,
                response: Response<ResponeDetailTopStory>
            ) {
                println("berhasil")
                println("Result:${response.body()}")

                val tittle = response.body()?.title
                val skor = response.body()?.score
                val komentar = response.body()?.descendants
            }
        })
    }
}


